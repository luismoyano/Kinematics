Kinematics is a Kinematics (duh!) simmulator developed in pure Android for learning purposes.

Features:

- MVC patterned
- Real time rendering using Android's native API
. Multithreading for calculations
- Customizable settings
- Projects' management system using JSON
- Not a single asset used, as lightweight as possible
- v7 and v4 compat libraries used, backwards compatibility guaranteed!