package com.kuma.kinematics;


import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

import com.kuma.kinematics.ParentFragment;
import com.kuma.kinematics.R;


public class AccelerationFragment extends ParentFragment {

    public static final String X_RECEIVED = "Xa got it!";
    public static final String Y_RECEIVED = "Ya got it!";

    public AccelerationFragment(){}

    public AccelerationFragment(boolean screen) {
        super(2, screen);

    }

    @Override
    public void onResume(){
        super.onResume();

        fragmentData.getxInputCartesian().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    try {
                        viewModel.setBodyAccelerationX(
                                Float.parseFloat(fragmentData.getxInputCartesian().getText().toString())
                        );
                    } catch (NumberFormatException e) {
                        fragmentData.getxInputCartesian().setHint(R.string.number_hint);
                        Toast.makeText(getActivity().getApplicationContext(), BAD_NUMBER, Toast.LENGTH_SHORT).show();
                    } catch (ArrayIndexOutOfBoundsException e) {
                        fragmentData.getxInputCartesian().setHint(R.string.number_hint);
                        Toast.makeText(getActivity().getApplicationContext(), NO_BODY, Toast.LENGTH_SHORT).show();
                    }
                    hideKeyboard(fragmentData.getV());
                    fragmentData.editingX = false;
                }
                return false;
            }
        });

        fragmentData.getyInputCartesian().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                fragmentData.editingY = true;
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    try {
                        viewModel.setBodyAccelerationY(
                                Float.parseFloat(fragmentData.getyInputCartesian().getText().toString())
                        );
                    } catch (NumberFormatException e) {
                        fragmentData.getyInputCartesian().setHint(R.string.number_hint);
                        Toast.makeText(getActivity().getApplicationContext(), BAD_NUMBER, Toast.LENGTH_SHORT).show();
                    } catch (ArrayIndexOutOfBoundsException e) {
                        fragmentData.getyInputCartesian().setHint(R.string.number_hint);
                        Toast.makeText(getActivity().getApplicationContext(), NO_BODY, Toast.LENGTH_SHORT).show();
                    }
                    hideKeyboard(fragmentData.getV());
                    fragmentData.editingY = false;
                }
                return false;
            }
        });

        fragmentData.getColorableV().post(new Runnable() {
            @Override
            public void run() {
                viewModel.getData().setAccColorableView(fragmentData.getColorableV());
            }
        });
    }
}