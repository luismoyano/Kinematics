package com.kuma.kinematics;

import android.graphics.PointF;
import android.graphics.Rect;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;


/**Controllers, View**/

public class CanvasGestureListener extends ScaleGestureDetector.SimpleOnScaleGestureListener implements GestureDetector.OnGestureListener {


    private KinematicsData data;



    private int selectedType;
    private int selectedIndex;

    private Rect touchableWorld;


    public CanvasGestureListener(Rect world) {
        //Log.e("CanvasGestureListener","Invoco Data");
        data = KViewModel.getInstancia().getData();
        touchableWorld = world;
        //Log.e("canvasGesture", "worldh" + touchableWorld.height());

    }

    @Override
    public boolean onDown(MotionEvent e) {

        if(data.getCuerpos().isEmpty()){
            data.getCuerpos().add(new Cuerpo(0)); data.getCuerpos().elementAt(data.getCuerpos().size() -1).index = data.getCuerpos().size() - 1;
            data.getCuerpos().add(new Cuerpo(1)); data.getCuerpos().elementAt(data.getCuerpos().size() -1).index = data.getCuerpos().size() - 1;
        }
        selectedType = selectedIndex = 0;
        for (int i = 1; i < data.getCuerpos().size(); i++) {
            if (data.getCuerpos().elementAt(i).contains((int)e.getX(), (int)e.getY())){
                selectedType = data.getCuerpos().elementAt(i).id;
                selectedIndex = data.getCuerpos().elementAt(i).index;
                break;
            }
        }
        Log.e("onDown", "SelectedType = " + selectedType);

        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        if(touchableWorld.contains((int)e.getX(), (int)e.getY())){
            //Nothing for now
        }
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {

        if (touchableWorld.contains((int) e2.getX(), (int) e2.getY())) {
            switch (selectedType) {
                case 0:
                    for (Cuerpo c : data.getCuerpos()) {
                        c.setPosicionCartesianaInicial(
                                new PointF(c.getPosicionCartesianaInicial().x - distanceX, c.getPosicionCartesianaInicial().y - distanceY),
                                distanceX, distanceY
                        );
                    }
                    data.setOrigen(data.getCuerpos().elementAt(0).getPosicionCartesianaInicial());
                    break;
                case 1://Cuerpo
                    switch (data.getSelected()) {
                        case 0:
                            data.getCuerpos().elementAt(selectedIndex).setPosicionCartesianaInicial(new PointF(e2.getX(), e2.getY()), distanceX, distanceY);
                            break;
                        case 1:
                            data.getCuerpos().elementAt(selectedIndex).vectorVelocidad.destino.set(e2.getX(), e2.getY());
                            data.getCuerpos().elementAt(selectedIndex).vectorVelocidad.setLongitud();

                            break;
                        case 2:
                            data.getCuerpos().elementAt(selectedIndex).vectorAceleracion.destino.set(e2.getX(), e2.getY());
                            data.getCuerpos().elementAt(selectedIndex).vectorAceleracion.setLongitud();
                            break;
                    }
                    break;
                default://Hopefully never happens
                    //Log.e("OnScroll", "");
                    break;

            }

        }

        return false;
    }

    @Override
    public void onLongPress(MotionEvent e) {

    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        return false;
    }

    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        super.onScale(detector);

        //Evitamos conflictos con onScroll
        selectedType = -1;

        float scale = detector.getScaleFactor();

        data.setTheoreticStep(data.getTheoreticStep() * scale);

        for (int i = 1; i < data.getCuerpos().size(); i++) {
            data.getCuerpos().elementAt(i).setPosicionCartesianaInicial(
                    data.getCuerpos().elementAt(i).teoricoAReal(
                            data.getCuerpos().elementAt(i).getPosicionCartesianaInicialTeorica()
                    ), 0, 0
            );

            data.getCuerpos().elementAt(i).vectorVelocidad.destino.set(
                    data.getCuerpos().elementAt(i).teoricoAReal(
                        data.getCuerpos().elementAt(i).vectorVelocidad.destinoTeorico
                    )
            );

            data.getCuerpos().elementAt(i).vectorAceleracion.destino.set(
                    data.getCuerpos().elementAt(i).teoricoAReal(
                            data.getCuerpos().elementAt(i).vectorAceleracion.destinoTeorico
                    )
            );


        }
        //Log.e("Canvas Listener", "uM = " + data.getUnidadMetrica());


        return true;
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector detector) {
        return super.onScaleBegin(detector);
    }

    @Override
    public void onScaleEnd(ScaleGestureDetector detector) {
        super.onScaleEnd(detector);
    }
}
