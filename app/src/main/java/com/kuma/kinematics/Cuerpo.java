package com.kuma.kinematics;

import android.graphics.PointF;
import android.graphics.RectF;
import android.util.Log;


public class Cuerpo {


    private KinematicsData data;

    public int dibujo;
    //0 Axis
    //1 Círculo
    //2 Rectángulo
    //More to come!

    public int id;
    public int index;

    public int radio;//En caso de círculo



    private PointF posicionCartesianaInicial = new PointF();//pixel coordinates
    private PointF posicionCartesianaInicialTeorica = new PointF();//theoretical coordinates


    public KinematicsVector vectorVelocidad;
    public KinematicsVector vectorAceleracion;

    public Cuerpo(int which) {
        //Log.e("Cuerpo", "Invoco Data!");
        data = KViewModel.getInstancia().getData();
        id = which;

        switch(id){
            case 0:
                //PosicionCartesianaInicial = Origen de coordenadas
                posicionCartesianaInicial.set(data.getSimmWorld().exactCenterX(), data.getSimmWorld().exactCenterY());
                data.setOrigen(posicionCartesianaInicial);
                break;

            case 1:
                radio = data.getUnidadMetrica() / 4;
                posicionCartesianaInicial.set(data.getOrigen());
                posicionCartesianaInicialTeorica = realATeorico(posicionCartesianaInicial);


                break;
        }
        vectorVelocidad = new KinematicsVector(this);
        vectorAceleracion = new KinematicsVector(this);

    }



    public boolean contains(int x, int y){
        return Math.sqrt(Math.pow(posicionCartesianaInicial.x - x, 2) + Math.pow(posicionCartesianaInicial.y - y, 2))
            <= radio * 2;//two times radius to make it easier for sloppy fingers to touch the body
    }


    public PointF getPosicionCartesianaInicial() {
        return posicionCartesianaInicial;
    }

    public void setPosicionCartesianaInicial(PointF posicionCartesianaInicial, float distanceX, float distanceY) {
        this.posicionCartesianaInicial = posicionCartesianaInicial;
        //posicionCartesianaInicialTeorica = realATeorico(posicionCartesianaInicial);

        vectorVelocidad.setDestinoFromadditive(distanceX, distanceY);

        vectorAceleracion.setDestinoFromadditive(distanceX, distanceY);
    }

    public PointF getPosicionCartesianaInicialTeorica() {
        return posicionCartesianaInicialTeorica;
    }

    public void setPosicionCartesianaInicialTeorica(PointF posicion) {
        this.posicionCartesianaInicialTeorica = posicion;
        this.posicionCartesianaInicial = teoricoAReal(posicion);

        //Move vectors also, a bit hacky, but does the trick
        vectorVelocidad.setDestinoTeorico(vectorVelocidad.getDestinoTeorico());
        vectorAceleracion.setDestinoTeorico(vectorAceleracion.getDestinoTeorico());
    }



    public PointF realATeorico(PointF posicionCartesianaInicial) {
        return new PointF(
                (posicionCartesianaInicial.x - data.getOrigen().x) * data.getTheoreticStep() / data.getUnidadMetrica(),
                (data.getOrigen().y - posicionCartesianaInicial.y) * data.getTheoreticStep() / data.getUnidadMetrica()
        );

        /**
        return new PointF(
                (posicionCartesianaInicial.x - data.getOrigen().x) / data.getUnidadMetrica(),
                (data.getOrigen().y - posicionCartesianaInicial.y) / data.getUnidadMetrica()
        );
         **/
    }

    public PointF teoricoAReal(PointF posicionTeorica){

        return new PointF(
                (posicionTeorica.x/data.getTheoreticStep() * data.getUnidadMetrica()) + data.getOrigen().x,
                ((posicionTeorica.y/data.getTheoreticStep() * data.getUnidadMetrica()) - data.getOrigen().y) * -1
        );
    }

    public void addPositionStep(PointF nextPosStep) {
        posicionCartesianaInicialTeorica = realATeorico(posicionCartesianaInicial);

        posicionCartesianaInicialTeorica.x += nextPosStep.x;
        posicionCartesianaInicialTeorica.y += nextPosStep.y;

        posicionCartesianaInicial = teoricoAReal(posicionCartesianaInicialTeorica);
    }

    public void addVelStep(PointF nextVelStep) {
        vectorVelocidad.destinoTeorico = nextVelStep;

        vectorVelocidad.destino =
                vectorVelocidad.teoricoAReal(vectorVelocidad.destinoTeorico);
    }
}
