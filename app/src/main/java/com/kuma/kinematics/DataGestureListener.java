package com.kuma.kinematics;

import android.graphics.Color;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.ViewFlipper;


/**CONTROL**/

public class DataGestureListener implements GestureDetector.OnGestureListener {



    //Identidad
    int id;

    KinematicsData data;


    public DataGestureListener(int id){
        //Log.e("DataGestureListener","Invoco el Data!");
        data = KViewModel.getInstancia().getData();
        this.id = id;
    }


    /**GestureDetector Methods**/
    @Override
    public boolean onDown(MotionEvent e) {
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {
        //Toast.makeText(getActivity().getBaseContext(), "onShowPress", Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        //Toast.makeText(getActivity().getBaseContext(), "onSingleTapUp", Toast.LENGTH_SHORT).show();
        data.setSelected(this.id);
        //Log.e("onSingleTapUp", "Selected = " + data.getSelected());
        switch (data.getSelected()){

            case 0:
                data.getPosColorableView().setBackgroundColor(data.getBodyColor());
                data.getPosColorableView().setAlpha(0.5f);
                data.getVelColorableView().setBackgroundColor(Color.TRANSPARENT);
                data.getAccColorableView().setBackgroundColor(Color.TRANSPARENT);

                //Log.e("onSingleTapUp", "POS");
                break;
            case 1:
                data.getPosColorableView().setBackgroundColor(Color.TRANSPARENT);
                data.getVelColorableView().setBackgroundColor(data.getVelColor());
                data.getVelColorableView().setAlpha(0.5f);
                data.getAccColorableView().setBackgroundColor(Color.TRANSPARENT);

                //Log.e("onSingleTapUp", "VEL");
                break;
            case 2:
                data.getPosColorableView().setBackgroundColor(Color.TRANSPARENT);
                data.getVelColorableView().setBackgroundColor(Color.TRANSPARENT);
                data.getAccColorableView().setBackgroundColor(data.getAccColor());
                data.getAccColorableView().setAlpha(0.5f);

                //Log.e("onSingleTapUp", "Acc");
                break;
        }

        return false;
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
        //Toast.makeText(getActivity().getBaseContext(), "onScroll", Toast.LENGTH_SHORT).show();
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        //Toast.makeText(getActivity().getBaseContext(), "onLongPress", Toast.LENGTH_SHORT).show();
        if(data.getSelected() == this.id){
            data.getPosColorableView().setBackgroundColor(Color.TRANSPARENT);
            data.getVelColorableView().setBackgroundColor(Color.TRANSPARENT);
            data.getAccColorableView().setBackgroundColor(Color.TRANSPARENT);
            data.setSelected(-1);
        }
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        //Toast.makeText(getActivity().getBaseContext(), "onFling", Toast.LENGTH_SHORT).show();

        return false;
    }
}
