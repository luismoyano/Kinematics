package com.kuma.kinematics;


import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v4.app.DialogFragment;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by Luis on 24/05/2015.
 */
public class FileOptionsDialog extends DialogFragment {

    public static final String fileNameStringKey = "fileKeyKeyKey!";
    public static final String fileOptionsTag = "Options";

    Context context;
    KViewModel viewModel = KViewModel.getInstancia();
    View view;

    private Button openButton;
    private Button saveButton;
    private Button deleteButton;

    String name = "";

    public FileOptionsDialog(){}//Android wants, Android gets

    public FileOptionsDialog(View v){

        context = getActivity();
        view = v;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        context = getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view);

        name = getArguments().getString(fileNameStringKey);

        openButton = (Button) view.findViewById(R.id.open_button);
        openButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.openFile(name);
                dismiss();
            }
        });

        saveButton = (Button) view.findViewById(R.id.save_button);
        saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.saveFile(name);
                dismiss();
            }
        });

        deleteButton = (Button) view.findViewById(R.id.delete_button);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewModel.deleteFile(name);
                dismiss();
            }
        });

        return builder.create();
    }

    @Override
    public void onDismiss(final DialogInterface dialog){
        super.onDismiss(dialog);

        final Activity activity = getActivity();
        if(activity instanceof DialogInterface.OnDismissListener){
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }
    }
}
