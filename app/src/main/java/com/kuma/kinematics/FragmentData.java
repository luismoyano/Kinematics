package com.kuma.kinematics;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PointF;
import android.os.Build;
import android.view.GestureDetector;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

/**
 * Created by Luis on 5/05/15.
 */
public class FragmentData{

    /**Getters**/
    public View getV() {return v;}
    public View getColorableV(){return colorableV;}
    public EditText getxInputCartesian() {return xInputCartesian;}
    public EditText getyInputCartesian() {return yInputCartesian;}

    public boolean editingX = false;
    public boolean editingY = false;

    /**Inputs**/
    private EditText xInputCartesian;
    private EditText yInputCartesian;


    /**View**/
    private View v;
    private ImageView colorableV;

    private KViewModel viewModel = KViewModel.getInstancia();

    /**Identidad de los hijos**/
    int hijo = 0;

    /**Titulo**/
    private TextView title;


    protected FragmentData(int which, LayoutInflater inflater, ViewGroup container){

        v =  inflater.inflate(R.layout.layout_1, container, false);
        colorableV = (ImageView) v.findViewById(R.id.colorable);
        colorableV.setFocusableInTouchMode(true);

        xInputCartesian = (EditText) v.findViewById(R.id.x_input_cart);
        yInputCartesian = (EditText) v.findViewById(R.id.y_input_cart);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {viewModel.pauseTimer();}};

        xInputCartesian.setOnClickListener(listener);
        yInputCartesian.setOnClickListener(listener);

        hijo = which;

        title = (TextView) v.findViewById(R.id.input_title);
        if(hijo > 0)title.setText(v.getResources().getString(hijo == 1?R.string.velocity:R.string.acceleration));

    }

    public void setXText(String t){
        if(!editingX) xInputCartesian.setText(t);
    }
    public void setYText(String t){
        if(!editingY)yInputCartesian.setText(t);
    }

    

}
