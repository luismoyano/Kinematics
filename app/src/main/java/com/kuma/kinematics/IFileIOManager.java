package com.kuma.kinematics;


public interface IFileIOManager {//Basic IO Operations

    void openFile(String name, KinematicsData data);
    void saveFile(KinematicsData data, String title);
    void deleteFile(String name);

    String[] getFileNames();

}
