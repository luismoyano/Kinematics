package com.kuma.kinematics;


import android.graphics.PointF;

public interface IPhysicsCalculatorManager {
    PointF getNextPosition(PointF vVector, PointF aVector);
    PointF getNextVelocity(PointF aVector, PointF vVector);

}
