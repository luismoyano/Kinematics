package com.kuma.kinematics;

import android.graphics.Canvas;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

/**
 * Created by Luis on 01/06/2015.
 */
public interface ISimmulation {
    void updateUIThread();
    void upDatePhysics();
    void drawSimmulation(Canvas canvas);
}
