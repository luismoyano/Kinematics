package com.kuma.kinematics;

public interface ITimeManager {
    void startTimer();
    void pauseTimer();
    void resumetimer();
    void stopTimer();
    boolean setTimerTo(String t);
    long getElapsedTime();
    String getFormattedTime();
    long updateTimer();

}
