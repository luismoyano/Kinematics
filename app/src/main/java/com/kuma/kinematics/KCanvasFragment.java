package com.kuma.kinematics;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by pandolet on 21/12/13.
 */
public class KCanvasFragment extends Fragment {

    public KinematicsCanvas getkCanvas() {
        return kCanvas;
    }

    private KinematicsCanvas kCanvas;

    private GestureDetector gestureDetector;
    private CanvasGestureListener d;
    private ScaleGestureDetector scaleGestureDetector;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        kCanvas = new KinematicsCanvas(getActivity().getBaseContext());

        d = new CanvasGestureListener(kCanvas.holder.getSurfaceFrame());

        gestureDetector = new GestureDetector(getActivity().getBaseContext(), d);
        scaleGestureDetector = new ScaleGestureDetector(getActivity().getBaseContext(), d);

        kCanvas.setOnTouchListener(new View.OnTouchListener(){
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getPointerCount() > 1) scaleGestureDetector.onTouchEvent(event);
                else {
                    if(scaleGestureDetector.isInProgress()) return true;
                    else gestureDetector.onTouchEvent(event);
                }

                return true;
            }
        });

        return kCanvas;
    }

    @Override
    public void onResume() {
        super.onResume();

        //Configuramos simmWorld
        kCanvas.post(new Runnable() {
            @Override
            public void run() {
                //Log.e("worldh = " + kCanvas.holder.getSurfaceFrame().height(), "worldh = " + kCanvas.holder.getSurfaceFrame().width());

                kCanvas.viewModel.getData().setSimmWorld(kCanvas.holder.getSurfaceFrame());
                kCanvas.viewModel.getData().setUnidadMetrica(setMetricUnits());
            }
        });
    }

    private int setMetricUnits(){//Método utilizado solo al inicio de un proyecto
        return (int)Math.min(kCanvas.viewModel.getData().getSimmWorld().height(), kCanvas.viewModel.getData().getSimmWorld().width()) / 10;
    }
}
