package com.kuma.kinematics;

import android.graphics.PointF;
import android.util.Log;

/**
 * Created by Dani on 30/05/2015.
 * A Really Really Really basic implementation of the kinematics physics
 */
public class KPhysicsCalculator {
    /**
     * We do know:
     *
     * x = Vi*t + (a*t^2)/2
     * Vf = Vi + a*t
     *
     * For now this is enough
     */

    public KPhysicsCalculator(){}//Empty Builder


    public PointF getNextPosition(PointF velocityVector, float deltaTime, PointF accelerationVector){
        float x = (float) ((velocityVector.x * deltaTime) + (accelerationVector.x * Math.pow(deltaTime, 2))/2);
        float y = (float) ((velocityVector.y * deltaTime) + (accelerationVector.y * Math.pow(deltaTime, 2))/2);

        //Log.e("KPhysics", "deltaTime = " + deltaTime);
        //Log.e("KPhysics", "PosStep X = " + x + ", Y = " + y);

        return new PointF(x,y);
    }

    public PointF getNextVelocity(PointF accelerationVector, PointF velocityVector, float deltaTime){
        float x = velocityVector.x + (accelerationVector.x * deltaTime);
        float y = velocityVector.y + (accelerationVector.y * deltaTime);

        //Log.e("KPhysics", "VelStep X = " + x + ", Y = " + y);

        return new PointF(x, y);
    }



}
