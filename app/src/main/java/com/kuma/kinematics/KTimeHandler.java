package com.kuma.kinematics;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;


/**
 * Created by Luis on 15/05/2015.
 */

//View for time handling
public class KTimeHandler {

    private final String BAD_FORMAT = "Wrong format; HH:MM:SS:sss != ";
    public static final String TIME_SET_CODE = "timey wimey!";

    private View timeView;
    private KViewModel viewModel;



    public EditText timeText;

    private Button stopButton;
    private Button playButton;
    private Button pauseButton;



    public KTimeHandler(View v){

        //Log.e("kTimeHandler", "Entro al constructor");

        timeView = v;
        viewModel = KViewModel.getInstancia();


        //Fill the gaps
        timeText = (EditText) timeView.findViewById(R.id.time_display);

        timeText.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_DOWN) {
                    if (keyCode == KeyEvent.KEYCODE_ENTER) {
                        if(!viewModel.setTimerTo(timeText.getText().toString())){
                            //Clear timeText
                            Toast.makeText(v.getContext(), BAD_FORMAT + timeText.getText().toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                return false;//Burn event propagation
            }
        });

        stopButton = (Button) timeView.findViewById(R.id.stop_button);
        playButton = (Button) timeView.findViewById(R.id.play_button);
        pauseButton = (Button) timeView.findViewById(R.id.pause_button);

        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("K TIME HANDLER", "STOP TIME!!");
                viewModel.stopTimer();
            }
        });
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("K TIME HANDLER", "PLAY TIME!!");
                if(viewModel.isTimePaused()){viewModel.resumeTimer();}
                else{viewModel.startTimer();}
            }
        });
        pauseButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Log.e("K TIME HANDLER", "PAUSE TIME!!");
                viewModel.pauseTimer();
            }
        });
    }

}
