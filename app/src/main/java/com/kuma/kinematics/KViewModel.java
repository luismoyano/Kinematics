package com.kuma.kinematics;

import android.content.Context;
import android.graphics.PointF;
import android.util.Log;

/**
 * Created by Luis on 20/05/2015.
 */

//Singletoned Kinematics ViewModel, contains a model instance + a KinematicsData instance
public class KViewModel {

    private KinematicsData data;
    private KinematicsModel model;

    private static KViewModel INSTANCIA;

    public static KViewModel getInstancia() {
        if (INSTANCIA == null) {
            synchronized (KViewModel.class) {
                if (INSTANCIA == null) {
                    INSTANCIA = new KViewModel();
                }
            }
        }
        return INSTANCIA;
    }

    public KViewModel() {
        //Log.e("KVIEWMODEL","ENTRO AL KVIEWMODEL!!");
        data = new KinematicsData();
        model = new KinematicsModel();
    }


    public KinematicsData getData() {return data;}


    //time, time, time in a bottle!
    public void startTimer(){model.startTimer();}
    public void pauseTimer(){model.pauseTimer();}
    public void resumeTimer(){model.resumetimer();}
    public void stopTimer(){model.stopTimer();}

    public boolean setTimerTo(String t){return model.setTimerTo(t);}

    public long getElapsedTime(){return model.getElapsedTime();}
    public String getFormattedTime(){return model.getFormattedTime();}

    public long updateTimer(){return model.updateTimer();}

    public boolean isTimePaused() {return model.isTimePaused();}
    public boolean isTimeStopped() {return model.isTimeStopped();}



    //files and saveState
    public void setContextForIO(Context context){model.setContextForIO(context);}

    public void saveFile(String name){model.saveFile(data, name);}
    public void openFile(String name){model.openFile(name, data);}
    public void deleteFile (String name){model.deleteFile(name);}

    public String getLifeCyclesSaver(){return model.getStringForSave(data);}
    public void retrieveSaving(String retrieved){model.fillData(retrieved, data);}

    public String[] getFileNames(){return model.getFileNames();}


    //Physics handling methods
    public void setBodyPositionX(float bodyPositionX) {
        data.getCuerpos().elementAt(1).setPosicionCartesianaInicialTeorica(
            new PointF(
                bodyPositionX,
                data.getCuerpos().elementAt(1).getPosicionCartesianaInicialTeorica().y
            )
        );
    }

    public void setBodyPositionY(float bodyPositionY) {
        data.getCuerpos().elementAt(1).setPosicionCartesianaInicialTeorica(
            new PointF(
                data.getCuerpos().elementAt(1).getPosicionCartesianaInicialTeorica().x,
                bodyPositionY
            )
        );
    }

    public void setBodyVelocityX(float bodyVelocityX) {
        data.getCuerpos().elementAt(1).vectorVelocidad.setDestinoTeorico(
                new PointF(
                        bodyVelocityX,
                        data.getCuerpos().elementAt(1).vectorVelocidad.getDestinoTeorico().y
                )
        );
    }

    public void setBodyVelocityY(float bodyVelocityY) {
        data.getCuerpos().elementAt(1).vectorVelocidad.setDestinoTeorico(
                new PointF(
                        data.getCuerpos().elementAt(1).vectorVelocidad.getDestinoTeorico().x,
                        bodyVelocityY
                )
        );
    }

    public void setBodyAccelerationX(float bodyAccelerationX) {
        data.getCuerpos().elementAt(1).vectorAceleracion.setDestinoTeorico(
            new PointF(
                bodyAccelerationX,
                data.getCuerpos().elementAt(1).vectorAceleracion.getDestinoTeorico().y
            )
        );

    }

    public void setBodyAccelerationY(float bodyAccelerationY) {
        data.getCuerpos().elementAt(1).vectorAceleracion.setDestinoTeorico(
            new PointF(
                data.getCuerpos().elementAt(1).vectorAceleracion.getDestinoTeorico().x,
                bodyAccelerationY
            )
        );
    }

    public void setNextSimmStep(){
        setPositionNextStep();
        setVelocityNextStep();
        updateAccVector();
    }

    private void setPositionNextStep(){
        PointF nextPosStep = model.getNextPosition(data.getCuerpos().elementAt(1).vectorVelocidad.getDestinoTeorico(),
                data.getCuerpos().elementAt(1).vectorAceleracion.getDestinoTeorico()
            );

        data.getCuerpos().elementAt(1).addPositionStep(nextPosStep);
    }

    private void setVelocityNextStep(){
        PointF nextVelStep = model.getNextVelocity(data.getCuerpos().elementAt(1).vectorAceleracion.getDestinoTeorico(),
                data.getCuerpos().elementAt(1).vectorVelocidad.getDestinoTeorico()
        );

        data.getCuerpos().elementAt(1).addVelStep(nextVelStep);
    }

    private void updateAccVector(){
        data.getCuerpos().elementAt(1).vectorAceleracion.destino =
                data.getCuerpos().elementAt(1).vectorAceleracion.teoricoAReal(
                        data.getCuerpos().elementAt(1).vectorAceleracion.destinoTeorico
                );
    }




    public boolean isProjectOn() {return !data.getCuerpos().isEmpty();}

    public void setColorPalette(String bodyColorPref, String velColorPref, String accColorPref) {
        data.setColorPalette(bodyColorPref, velColorPref, accColorPref);
    }

    public void shouldRenderFPS(boolean sharedPrefBoolean) {
        data.setRenderFPS(sharedPrefBoolean);
    }

    public boolean resetProject() {
        if(!isProjectOn()) {return false;}
        String tempSave = "saved on overwrite";
        model.saveFile(data,tempSave);

        //reset time
        stopTimer();

        //reset body:
        data.getCuerpos().elementAt(1).setPosicionCartesianaInicialTeorica(new PointF(0, 0));
        data.getCuerpos().elementAt(1).vectorVelocidad.setDestinoTeorico(new PointF(0, 0));
        data.getCuerpos().elementAt(1).vectorAceleracion.setDestinoTeorico(new PointF(0,0));

        data.setSelected(-1);

        data.setTheoreticStep(1);

        data.setOrigen(
                data.getCuerpos().elementAt(1).getPosicionCartesianaInicial()
        );
        return true;
    }
}