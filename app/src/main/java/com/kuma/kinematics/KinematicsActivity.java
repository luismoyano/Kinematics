package com.kuma.kinematics;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//View Class
public class KinematicsActivity extends ActionBarActivity implements DialogInterface.OnDismissListener{

    private final String selectDrawer = "Select For more options";
    private final String SAVE_STATE_KEY = "SaveySavey!:D";

    private final int SETTINGS_RESULT_CODE = 12345;
    private String bodyColorPref = "";
    private String velColorPref = "";
    private String accColorPref = "";


    KCanvasFragment KCanvas = new KCanvasFragment();

    PositionFragment posFragment;
    VelocityFragment velFragment;
    AccelerationFragment accFragment;

    android.support.v7.app.ActionBar actionBar;

    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private ListView filesList;


    private KViewModel viewModel;

    public KTimeHandler timeHandler;

    private FileOptionsDialog filesDialog;
    private SaveDialog saveDialog;

    private UIHandler uiHandler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Toast.makeText(this, "KinematicsOnCreate", Toast.LENGTH_SHORT).show();

        if(savedInstanceState != null){
            String saved = (String)savedInstanceState.get(SAVE_STATE_KEY);
            //if(saved != null) Log.e("KinematicsActivity", saved);
        }
        else{/**Log.e("KinematicsActivity", "Esto es un first Build!!!");**/}


        viewModel = KViewModel.getInstancia();

        setContentView(R.layout.activity_kinematics);

        actionBar = getSupportActionBar();

        posFragment = new PositionFragment(findViewById(R.id.data_container) != null);
        velFragment = new VelocityFragment(findViewById(R.id.data_container) != null);
        accFragment = new AccelerationFragment(findViewById(R.id.data_container) != null);



        if (findViewById(R.id.data_container) != null) {//Estamos en pantalla pequeña,

            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);



            ActionBar.TabListener tabListener = new ActionBar.TabListener() {
                @Override
                public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {

                    if(tab.getText().equals(getResources().getString(R.string.Simulation))){
                        ft = getSupportFragmentManager().beginTransaction();
                        getSupportFragmentManager().executePendingTransactions();
                        ft.remove(posFragment);
                        ft.remove(velFragment);
                        ft.remove(accFragment);

                        ft.add(R.id.kcanvas_container, KCanvas);
                        ft.commit();
                    }
                    else if(tab.getText().equals(getResources().getString(R.string.Data))){//Es DATA
                        ft = getSupportFragmentManager().beginTransaction();
                        getSupportFragmentManager().executePendingTransactions();
                        ft.remove(KCanvas);

                        ft.add(R.id.data_container, accFragment);
                        ft.add(R.id.data_container, velFragment);
                        ft.add(R.id.data_container, posFragment);
                        ft.commit();
                    }
                }

                @Override
                public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                    //Nothing
                }

                @Override
                public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
                    //Nothing
                }
            };


            actionBar.addTab(actionBar.newTab().setText(R.string.Simulation).setTabListener(tabListener));
            actionBar.addTab(actionBar.newTab().setText(R.string.Data).setTabListener(tabListener));

        }
        else{setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);}

        // Navigation Drawer
        viewModel.setContextForIO(getApplicationContext());

        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        filesList = (ListView) findViewById(R.id.left_drawer);

        filesList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bundle bundle = new Bundle();
                bundle.putString(FileOptionsDialog.fileNameStringKey, ((TextView) view.findViewById(android.R.id.text1)).getText().toString());

                filesDialog = new FileOptionsDialog(View.inflate(getApplicationContext(), R.layout.dialog_layout, null));
                filesDialog.setArguments(bundle);

                filesDialog.show(getSupportFragmentManager(), FileOptionsDialog.fileOptionsTag);

            }
        });

        drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, R.mipmap.ic_navigation_drawer,
                R.string.drawer_open, R.string.drawer_close) {
            public void onDrawerClosed(View view) {
                getSupportActionBar().setTitle(getTitle());
            }//Titulo App

            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(selectDrawer);
                setFilesList();

            }
        };
        drawerLayout.setDrawerListener(drawerToggle);
        actionBar.setDisplayHomeAsUpEnabled(true);

        timeHandler = new KTimeHandler(findViewById(R.id.time_handler));

        uiHandler = new UIHandler(new WeakReference<>(this));
        viewModel.getData().setUiHandler(uiHandler);

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        //Toast.makeText(this, "KinematicsOnRestoreInstanceState", Toast.LENGTH_SHORT).show();

        viewModel.retrieveSaving(savedInstanceState.getString(SAVE_STATE_KEY));
    }

    @Override
    protected void onResume(){
        super.onResume();
        //Toast.makeText(this, "KinematicsOnResume", Toast.LENGTH_SHORT).show();

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();

        retrievePreferences();
        if(findViewById(R.id.container_large) != null) {

            ft.add(R.id.container_large, accFragment);
            ft.add(R.id.container_large, velFragment);
            ft.add(R.id.container_large, posFragment);
            ft.commit();

        }
        else{
            if(KCanvas.getkCanvas() != null){
                KCanvas.getkCanvas().invalidate();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        //Toast.makeText(this, "KinematicsOnSaveInstanceState", Toast.LENGTH_SHORT).show();

        String stateToBeSaved = viewModel.getLifeCyclesSaver();
        outState.putString(SAVE_STATE_KEY, stateToBeSaved);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Toast.makeText(this,"KinematicsOnPause",Toast.LENGTH_SHORT).show();
        if(KCanvas.getkCanvas() != null){
            KCanvas.getkCanvas().renderThread.interrupt();
        }
        viewModel.pauseTimer();

        if(findViewById(R.id.container_large) != null) {
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.remove(posFragment);
            ft.remove(velFragment);
            ft.remove(accFragment);
            ft.commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.kinematics, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id){
            case R.id.action_settings:
                Intent intent = new Intent(this, KSettingsActivity.class);
                startActivityForResult(intent, SETTINGS_RESULT_CODE);
                return true;

            case R.id.save_current_project:
                saveDialog = new SaveDialog(View.inflate(this, R.layout.save_dialog_layout, null));
                saveDialog.show(getSupportFragmentManager(), SaveDialog.saveTag);
                break;

            case R.id.new_project:
                if(!viewModel.resetProject()){
                    Toast.makeText(this, getResources().getString(R.string.reset_project), Toast.LENGTH_SHORT).show();
                }
                break;
        }



        if (drawerToggle.onOptionsItemSelected(item)) {return true;}

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    private void setFilesList(){
        String[] names = viewModel.getFileNames();
        List<String> namesList = new ArrayList<String>(Arrays.asList(names));

        filesList.setAdapter(new ArrayAdapter<>(
                getApplicationContext(),
                android.R.layout.simple_list_item_1,
                namesList
        ));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SETTINGS_RESULT_CODE){//We just came back from settings screen, time to retrieve and update!!
            retrievePreferences();
        }
    }

    private void retrievePreferences(){

        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        //sharedPref.edit().clear().commit();
        bodyColorPref = sharedPref.getString(getResources().getString(R.string.body_key), getResources().getStringArray(R.array.pref_color_values)[0]);
        velColorPref = sharedPref.getString(getResources().getString(R.string.vel_key), getResources().getStringArray(R.array.pref_color_values)[0]);
        accColorPref = sharedPref.getString(getResources().getString(R.string.acc_key), getResources().getStringArray(R.array.pref_color_values)[0]);

        //Log.e("KinematicsActivity","Preferences Retrieved " + bodyColorPref + " " + velColorPref + " " + accColorPref);

        viewModel.setColorPalette(
                bodyColorPref,
                velColorPref,
                accColorPref
        );

        viewModel.shouldRenderFPS(sharedPref.getBoolean(getResources().getString(R.string.pref_key_fps), false));
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        setFilesList();//Update list on any case!
    }
}
