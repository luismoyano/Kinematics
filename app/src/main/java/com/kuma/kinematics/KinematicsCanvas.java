package com.kuma.kinematics;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.LinkedList;


/**VIEW**/

public class KinematicsCanvas extends SurfaceView implements Runnable, ISimmulation {

    private final String goGoGo = "Let's Start!";

    Thread renderThread = null;
    public boolean reproducir = true;
    SurfaceHolder holder;
    Paint pincel;

    Bundle UIBundle;
    Message message;

    KViewModel viewModel = KViewModel.getInstancia();

    /**Contar Frames**/
    private final int MAX_SIZE = 100;
    private final double NANOS = 1000000000.0;
    private LinkedList<Long> times = new LinkedList<Long>();
    private boolean renderFPS = false;//By Default


    public KinematicsCanvas(Context context){
        super(context);
        iniciar();
    }

    public KinematicsCanvas(Context context, AttributeSet attrs){
        super(context, attrs);
        iniciar();
    }
    public KinematicsCanvas ( Context context , AttributeSet attrs , int defStyle ){
        super(context, attrs, defStyle);
        iniciar();
    }


    void iniciar(){

        this.holder = getHolder();
        pincel = new Paint();
        pincel.setAntiAlias(true);
        pincel.setColor(Color.BLUE);
        pincel.setTextAlign(Paint.Align.CENTER);
        pincel.setTextScaleX(0.5f);//OJO AQUI

        holder.getSurfaceFrame();


        renderThread = new Thread(this);
        renderThread.start();

        UIBundle = new Bundle();

    }


    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        //backGround
        canvas.drawColor(Color.WHITE);
        if(viewModel.isProjectOn()) {
            for (int i = 0; i < viewModel.getData().getCuerpos().size(); i++) {
                Cuerpo cuerpo = viewModel.getData().getCuerpos().elementAt(i);

                switch (cuerpo.id) {

                    case 0: //Axis

                        int numbersOffset = canvas.getHeight() / 30;


                        pincel.setColor(Color.BLACK);
                        pincel.setStrokeWidth(1f);
                        pincel.setTextSize(numbersOffset);

                        canvas.drawLine(0, viewModel.getData().getOrigen().y, canvas.getWidth(), viewModel.getData().getOrigen().y, pincel);
                        canvas.drawLine(viewModel.getData().getOrigen().x, 0, viewModel.getData().getOrigen().x, canvas.getHeight(), pincel);

                        //Bucle para dibujar unidades métricas en X
                        int distancia = Math.max((int) viewModel.getData().getOrigen().x, (int) (canvas.getWidth() - viewModel.getData().getOrigen().x));

                        int offset = 0;
                        float step = 1;

                        int virtualStep = Math.max(1,
                                (numbersOffset * String.format("%.2f", viewModel.getData().getTheoreticStep()).length()
                                        /viewModel.getData().getUnidadMetrica()));
                        int stepBuffer = virtualStep;

                        while (distancia > viewModel.getData().getUnidadMetrica()) {
                            offset += viewModel.getData().getUnidadMetrica();

                            canvas.drawLine(viewModel.getData().getOrigen().x + offset, viewModel.getData().getOrigen().y,
                                    viewModel.getData().getOrigen().x + offset, viewModel.getData().getOrigen().y + numbersOffset, pincel);//Marcas a la derecha


                            canvas.drawLine(viewModel.getData().getOrigen().x - offset, viewModel.getData().getOrigen().y,
                                    viewModel.getData().getOrigen().x - offset, viewModel.getData().getOrigen().y + numbersOffset, pincel);//marcas a la izquierda


                            if(step >= stepBuffer) {
                                canvas.drawText(String.format("%.2f", step * viewModel.getData().getTheoreticStep()),
                                        viewModel.getData().getOrigen().x + offset, viewModel.getData().getOrigen().y + numbersOffset * 2, pincel);

                                canvas.drawText(String.format("%.2f", -step * viewModel.getData().getTheoreticStep()),
                                        viewModel.getData().getOrigen().x - offset, viewModel.getData().getOrigen().y + numbersOffset * 2 , pincel);

                                stepBuffer += virtualStep;
                            }



                            distancia -= viewModel.getData().getUnidadMetrica();
                            step++;

                        }

                        offset = 0;
                        step = 1;
                        virtualStep = Math.max(1,
                                (numbersOffset * (""+viewModel.getData().getTheoreticStep()).length()
                                        /viewModel.getData().getUnidadMetrica()));
                        stepBuffer = virtualStep;

                        distancia = Math.max((int) viewModel.getData().getOrigen().y, (int) (canvas.getHeight() - viewModel.getData().getOrigen().y));
                        while (distancia > viewModel.getData().getUnidadMetrica()) {
                            offset += viewModel.getData().getUnidadMetrica();

                            canvas.drawLine(viewModel.getData().getOrigen().x, viewModel.getData().getOrigen().y - offset,
                                    viewModel.getData().getOrigen().x + numbersOffset, viewModel.getData().getOrigen().y - offset, pincel);//Marcas hacia arriba

                            canvas.drawLine(viewModel.getData().getOrigen().x, viewModel.getData().getOrigen().y + offset,
                                    viewModel.getData().getOrigen().x + numbersOffset, viewModel.getData().getOrigen().y + offset, pincel);//marcas hacia abajo


                            if(step >= stepBuffer) {
                                canvas.drawText(String.format("%.2f", step * viewModel.getData().getTheoreticStep()),
                                        viewModel.getData().getOrigen().x + numbersOffset * 2, viewModel.getData().getOrigen().y - offset, pincel);

                                canvas.drawText(String.format("%.2f", -step * viewModel.getData().getTheoreticStep()),
                                        viewModel.getData().getOrigen().x + numbersOffset * 2, viewModel.getData().getOrigen().y + offset, pincel);

                                stepBuffer += virtualStep;
                            }

                            distancia -= viewModel.getData().getUnidadMetrica();
                            step++;
                        }

                        break;

                    case 1: //Circle
                        //Log.e("onDraw","Dibujo círculo");
                        pincel.setColor(viewModel.getData().getBodyColor());
                        canvas.drawCircle(cuerpo.getPosicionCartesianaInicial().x, cuerpo.getPosicionCartesianaInicial().y, cuerpo.radio, pincel);

                        //Vector de velocidad
                        pincel.setColor(viewModel.getData().getVelColor());
                        pincel.setStrokeWidth(cuerpo.radio/3);
                        canvas.drawLine(cuerpo.getPosicionCartesianaInicial().x, cuerpo.getPosicionCartesianaInicial().y, cuerpo.vectorVelocidad.getDestino().x,
                                cuerpo.vectorVelocidad.getDestino().y, pincel);

                        //Vector de aceleración
                        pincel.setColor(viewModel.getData().getAccColor());
                        canvas.drawLine(cuerpo.getPosicionCartesianaInicial().x, cuerpo.getPosicionCartesianaInicial().y, cuerpo.vectorAceleracion.getDestino().x,
                                cuerpo.vectorAceleracion.getDestino().y, pincel);

                        break;
                }
            }
        }
        else{
            pincel.setTextSize(canvas.getHeight() / 10);
            canvas.drawText(goGoGo, canvas.getWidth() / 2, canvas.getHeight() / 2, pincel);
        }

        pincel.setTextSize(canvas.getHeight() / 10);
        if(viewModel.getData().isRenderFPS()) {canvas.drawText(Math.round(fps()) + " fps", 100, 100, pincel);}
    }


    //Thread!!
    @Override
    public void run() {
        while(reproducir){

            if(!holder.getSurface().isValid()) continue;

            Canvas canvas = holder.lockCanvas(null);
            if(canvas != null) {


                if(viewModel.isProjectOn()){
                    upDatePhysics();
                    if(!viewModel.isTimePaused() && !viewModel.isTimeStopped()) {updateUIThread();}//UIThread update
                }

                drawSimmulation(canvas);//Drawing update

                if(!holder.isCreating()){holder.unlockCanvasAndPost(canvas);}
            }
        }
    }

    public void updateUIThread(){

        viewModel.updateTimer();

        //Handlers for time updating in order of avoiding memory leaks.
        message = new Message();
        UIBundle.clear();//Clear previous data
        UIBundle.putString(KTimeHandler.TIME_SET_CODE, viewModel.getFormattedTime());


        UIBundle.putFloat(PositionFragment.X_RECEIVED, viewModel.getData().getCuerpos().elementAt(1).getPosicionCartesianaInicialTeorica().x);
        UIBundle.putFloat(PositionFragment.Y_RECEIVED, viewModel.getData().getCuerpos().elementAt(1).getPosicionCartesianaInicialTeorica().y);

        UIBundle.putFloat(VelocityFragment.X_RECEIVED, viewModel.getData().getCuerpos().elementAt(1).vectorVelocidad.destinoTeorico.x);
        UIBundle.putFloat(VelocityFragment.Y_RECEIVED, viewModel.getData().getCuerpos().elementAt(1).vectorVelocidad.destinoTeorico.y);

        UIBundle.putFloat(AccelerationFragment.X_RECEIVED, viewModel.getData().getCuerpos().elementAt(1).vectorAceleracion.destinoTeorico.x);
        UIBundle.putFloat(AccelerationFragment.Y_RECEIVED, viewModel.getData().getCuerpos().elementAt(1).vectorAceleracion.destinoTeorico.y);

        UIBundle.putBoolean(UIHandler.shouldUpdate, true);



        message.setData(UIBundle);

        Handler onUIPlease = new Handler(Looper.getMainLooper());
        onUIPlease.post(new Runnable() {
            @Override
            public void run() {
                viewModel.getData().getUiHandler().handleMessage(message);
            }
        });

    }
    public void upDatePhysics(){viewModel.setNextSimmStep();}
    public void drawSimmulation(Canvas canvas){
        draw(canvas);
    }

    private double fps() {
        long lastTime = System.nanoTime();
        times.addLast(lastTime);
        double difference = (lastTime - times.getFirst()) / NANOS;
        int size = times.size();
        if (size > MAX_SIZE) {
            times.removeFirst();
        }
        return difference > 0 ? times.size() / difference : 0.0;
    }

}
