package com.kuma.kinematics;

import android.graphics.Color;
import android.graphics.PointF;
import android.graphics.Rect;
import android.view.View;

import java.util.Vector;


/**Global class for getters and setters, part of ViewModel**/

public class KinematicsData {

    private boolean renderFPS;

    public KinematicsData() {}//Empty builder

    //Valores iniciales
    private int selected = -1;//0 = pos, 1 = vel, 2 = acc.

    private View posColorableView;
    private View velColorableView;
    private View accColorableView;

    private UIHandler uiHandler;

    /**Cuerpos que participan en la simulación**/
    private Vector<Cuerpo> cuerpos = new Vector<Cuerpo>();


    /**Distancia predeterminada en pixeles para unidad métrica seleccionada**/
    private int unidadMetrica;


    /**Steps teoricos en el plano cartesiano**/
    private float theoreticStep = 1f;//by Default

    /**Mundo de simulación**/
    private Rect simmWorld;


    /**Origen de coordenadas (punto 0,0)**/
    private PointF origen;


    /**Colores para simulacion**/
    private int bodyColor;
    private int velColor;
    private int accColor;


    public PointF getOrigen() {
        return origen;
    }

    public void setOrigen(PointF origen) {
        this.origen = origen;
    }

    public Rect getSimmWorld() {
        return simmWorld;
    }

    public void setSimmWorld(Rect simmWorld) {
        this.simmWorld = simmWorld;
    }

    public int getUnidadMetrica() {
        return unidadMetrica;
    }

    public void setUnidadMetrica(int unidadMetrica) {
        this.unidadMetrica = unidadMetrica;

        if(this.unidadMetrica <= 10) this.unidadMetrica = 10;
        int cotaSup = Math.min(simmWorld.width(), simmWorld.height()) / 2;
        if(this.unidadMetrica >= cotaSup){this.unidadMetrica = cotaSup - 5;}
    }

    public float getTheoreticStep() {return theoreticStep;}
    public void setTheoreticStep(float theoreticStep) {this.theoreticStep = theoreticStep;}

    public Vector<Cuerpo> getCuerpos() {
        return cuerpos;
    }

    public int getSelected() {return selected;}
    public void setSelected(int x) {
        selected = x;
    }

    public View getPosColorableView() {
        return posColorableView;
    }
    public void setPosColorableView(View posColorableView) {
        this.posColorableView = posColorableView;
    }
    public View getVelColorableView() {return velColorableView;}
    public void setVelColorableView(View velColorableView) {this.velColorableView = velColorableView;}

    public View getAccColorableView() {
        return accColorableView;
    }
    public void setAccColorableView(View accColorableView) {this.accColorableView = accColorableView;}

    public UIHandler getUiHandler() {return uiHandler;}
    public void setUiHandler(UIHandler uiHandler) {
        this.uiHandler = uiHandler;
    }

    public void setColorPalette(String bodyColorPref, String velColorPref, String accColorPref) {
        bodyColor = Color.parseColor(bodyColorPref);
        velColor = Color.parseColor(velColorPref);
        accColor =  Color.parseColor(accColorPref);
    }

    public int getBodyColor() {return bodyColor;}
    public int getVelColor() {return velColor;}
    public int getAccColor() {return accColor;}

    public boolean isRenderFPS() {return renderFPS;}
    public void setRenderFPS(boolean renderFPS) {this.renderFPS = renderFPS;}


}
