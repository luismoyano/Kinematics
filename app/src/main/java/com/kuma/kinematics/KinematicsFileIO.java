package com.kuma.kinematics;

import android.content.Context;
import android.graphics.PointF;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Dani on 24/05/2015.
 * Includes JSON parser methods for writting and reading + IOStreams in order to write and read appropiately outside application context
 */
public class KinematicsFileIO {

    private String internalPath;
    private final String extension = ".json";

    private final String currentSelected = "selected";

    private final String edgesX = "edgesX";
    private final String edgesY = "edgesY";

    private final String body = "body";
    private final String bodyPosX = "bodyPosX";
    private final String bodyPosY = "bodyPosY";

    private final String bodyVelX = "bodyVelX";
    private final String bodyVelY = "bodyVelY";

    private final String bodyAccX = "bodyAccX";
    private final String bodyAccY = "bodyAccY";

    private final String origin = "cartesianOrigin";
    private final String unidadMetrica = "uM";
    private final String theoreticStep = "theoreticStep";


    private Context context;

    private JSONObject kJson;

    private JSONObject edgesSubObject;
    private JSONObject bodySubObject;

    private File savedFile;

    public KinematicsFileIO(){}//Empty builder in case we want to pass context later

    public KinematicsFileIO(Context context) {
        setUpByContext(context);
    }

    public void setUpByContext(Context c){
        this.context = c;
        internalPath = context.getFilesDir().getAbsolutePath();
    }

    public void saveFile(KinematicsData data, String title) {

        String savedData = createNewKJSONObject(data);
        String fullPath;
        if (savedData != null) {//just to make sure
            try {
                fullPath = internalPath + "/" + title;
                if(!title.contains(extension)){fullPath+=extension;}

                savedFile = new File(fullPath);
                FileOutputStream fos = new FileOutputStream(savedFile);
                fos.write(savedData.getBytes());
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    public void openFile(String fileName, KinematicsData data) {
        try {
            String retrieved = "";

            FileInputStream fis = new FileInputStream(internalPath + "/" + fileName);
            DataInputStream in = new DataInputStream(fis);
            BufferedReader br = new BufferedReader(new InputStreamReader(in));
            String strLine;
            while ((strLine = br.readLine()) != null) {
                retrieved += strLine;
            }
            in.close();

            //Log.e("FileIO",retrieved);
            fillDataByJSONObject(retrieved, data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void deleteFile(String fileName) {
        context.deleteFile(fileName);//filename only
    }


    public String[] getFileNames() {
        File f = new File(internalPath);
        return f.list();
    }


    public String createNewKJSONObject(KinematicsData data) {
        try {
            kJson = new JSONObject();

            //Setteamos al seleccionado actual
            kJson.put(currentSelected, data.getSelected());

            //Setteamos el origen de coordenadas
            edgesSubObject = new JSONObject();
            edgesSubObject.put(edgesX, data.getOrigen().x);
            edgesSubObject.put(edgesY, data.getOrigen().y);

            kJson.put(origin, edgesSubObject);

            //Setteamos unidad metrica
            kJson.put(unidadMetrica, data.getUnidadMetrica());
            kJson.put(theoreticStep, data.getTheoreticStep());

            //Setteamos cuerpo
            bodySubObject = new JSONObject();

            bodySubObject.put(bodyPosX, data.getCuerpos().elementAt(1).getPosicionCartesianaInicialTeorica().x);
            bodySubObject.put(bodyPosY, data.getCuerpos().elementAt(1).getPosicionCartesianaInicialTeorica().y);

            bodySubObject.put(bodyVelX, data.getCuerpos().elementAt(1).vectorVelocidad.getDestinoTeorico().x);
            bodySubObject.put(bodyVelY, data.getCuerpos().elementAt(1).vectorVelocidad.getDestinoTeorico().y);

            bodySubObject.put(bodyAccX, data.getCuerpos().elementAt(1).vectorAceleracion.getDestinoTeorico().x);
            bodySubObject.put(bodyAccY, data.getCuerpos().elementAt(1).vectorAceleracion.getDestinoTeorico().y);

            kJson.put(body, bodySubObject);


            //Log.e("KFILEIO", "Guardamos = " + kJson.toString());

            return kJson.toString();

        } catch (JSONException e) {
            //Lololol I don't know!
        }
        return null;
    }

    public void fillDataByJSONObject(String JSONString, KinematicsData data) {
        try {
            kJson = new JSONObject(JSONString);

            //Log.e("KFILEIO", "Retrieve! = " + kJson.toString());

            //CurrentSelectedOne
            data.setSelected(kJson.getInt(currentSelected));

            //CoordinatesOrigin
            edgesSubObject = kJson.getJSONObject(origin);
            data.setOrigen(
                    new PointF(
                            (float) edgesSubObject.getDouble(edgesX),
                            (float) edgesSubObject.getDouble(edgesY)
                    )
            );

            //MetricUnit
            data.setUnidadMetrica(kJson.getInt(unidadMetrica));
            data.setTheoreticStep((float)kJson.getDouble(theoreticStep));

            //Set body
            bodySubObject = kJson.getJSONObject(body);

            data.getCuerpos().elementAt(1).setPosicionCartesianaInicialTeorica(
                    new PointF(
                            (float) bodySubObject.getDouble(bodyPosX),
                            (float) bodySubObject.getDouble(bodyPosY)
                    )
            );

            KinematicsVector vel = new KinematicsVector(data.getCuerpos().elementAt(1));
            vel.setDestinoTeorico(
                    new PointF(
                            (float) bodySubObject.getDouble(bodyVelX),
                            (float) bodySubObject.getDouble(bodyVelY)
                    )
            );

            KinematicsVector acc = new KinematicsVector(data.getCuerpos().elementAt(1));
            acc.setDestinoTeorico(
                    new PointF(
                            (float) bodySubObject.getDouble(bodyAccX),
                            (float) bodySubObject.getDouble(bodyAccY)
                    )
            );

            data.getCuerpos().elementAt(1).vectorVelocidad = vel;
            data.getCuerpos().elementAt(1).vectorAceleracion = acc;

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (NullPointerException e){
            //Nothing here
        }
    }
}