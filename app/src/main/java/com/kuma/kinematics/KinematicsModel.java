package com.kuma.kinematics;

import android.content.Context;
import android.graphics.PointF;

/**
 * Created by Luis on 20/05/2015.
 * Manages Time, File IO, Physics Calculations
 */
public class KinematicsModel implements ITimeManager, IFileIOManager, IPhysicsCalculatorManager {

    public KinematicsWatch kWatch;
    public KinematicsFileIO fileIO;
    public KPhysicsCalculator physicsCalculator;

    public KinematicsModel(){
        //Log.e("MODEL","ENTRO AL CONSTRUCTOR");
        kWatch = new KinematicsWatch();
        fileIO = new KinematicsFileIO();
        physicsCalculator = new KPhysicsCalculator();
    }


    //TimeManager
    public void startTimer(){kWatch.start();}
    public void pauseTimer(){kWatch.pause();}
    public void resumetimer(){kWatch.resume();}
    public void stopTimer(){kWatch.stop();}

    public boolean setTimerTo(String t){return kWatch.setTime(t);}

    public long getElapsedTime(){return kWatch.getElapsedTime();}
    public String getFormattedTime(){return kWatch.getFormattedTime();}

    public long updateTimer(){return kWatch.updateTime();}

    public boolean isTimePaused() {return kWatch.isPaused();}
    public boolean isTimeStopped() {return kWatch.isStopped();}



    //IO Manager
    public void setContextForIO(Context c){fileIO.setUpByContext(c);}

    public void saveFile(KinematicsData data, String title){fileIO.saveFile(data, title);}

    public void openFile(String name, KinematicsData data){fileIO.openFile(name, data);}

    public void deleteFile(String name){fileIO.deleteFile(name);}

    public String getStringForSave(KinematicsData data){return fileIO.createNewKJSONObject(data);}
    public void fillData(String retrieved, KinematicsData data){fileIO.fillDataByJSONObject(retrieved, data);}

    public String[] getFileNames(){return fileIO.getFileNames();}


    //Physics manager
    public PointF getNextPosition(PointF vVector, PointF aVector){return physicsCalculator.getNextPosition(vVector, kWatch.getDeltaSeconds(), aVector);}
    public PointF getNextVelocity(PointF aVector, PointF vVector){return physicsCalculator.getNextVelocity(aVector, vVector, kWatch.getDeltaSeconds());}

}