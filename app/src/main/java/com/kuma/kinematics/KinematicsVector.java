package com.kuma.kinematics;

import android.graphics.PointF;



//Clase destinada a vectores de velocidad y aceleración
public class KinematicsVector {


    private KinematicsData data = KViewModel.getInstancia().getData();


    //Utilidades físicas
    private float thetaTeorico;//Angulo
    private float rhoTeorico;//Modulo
    private float longitudTeorica;//No sé si sería lo mismo que el módulo

    //Utilidades de dibujado

    public PointF destino = new PointF();
    public PointF destinoTeorico = new PointF();


    public PointF longitudXY = new PointF();

    private Cuerpo padre;
    //public int color;

    public KinematicsVector(Cuerpo padre){
        this.padre = padre;
        destino.set(padre.getPosicionCartesianaInicial());
    }


    public void setLongitud(){
        longitudXY.x = destino.x - padre.getPosicionCartesianaInicial().x;
        longitudXY.y = destino.y - padre.getPosicionCartesianaInicial().y;

        destinoTeorico = realATeorico(destino);
    }

    public PointF getDestino(){
        return destino;
    }

    public PointF getDestinoTeorico(){return destinoTeorico;}


    public void setDestinoTeorico(PointF destinoTeorico) {
        this.destinoTeorico = destinoTeorico;
        this.destino = teoricoAReal(this.destinoTeorico);

    }



    public void setDestinoFromadditive(float x, float y){
        destino.x -= x;
        destino.y -= y;
    }



    public PointF realATeorico(PointF posicionCartesianaInicial) {
        return new PointF(
                (posicionCartesianaInicial.x - padre.getPosicionCartesianaInicial().x) * data.getTheoreticStep()  / data.getUnidadMetrica(),
                (padre.getPosicionCartesianaInicial().y - posicionCartesianaInicial.y) * data.getTheoreticStep() / data.getUnidadMetrica()
        );
    }

    public PointF teoricoAReal(PointF posicionTeorica){
        return new PointF(
                (posicionTeorica.x/data.getTheoreticStep() * data.getUnidadMetrica()) + padre.getPosicionCartesianaInicial().x,
                ((posicionTeorica.y/data.getTheoreticStep() * data.getUnidadMetrica()) - padre.getPosicionCartesianaInicial().y) * -1
        );
    }



}
