package com.kuma.kinematics;


import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KinematicsWatch {

    private String FORMAT = "HH:mm:ss:SSS";

    private long deltaTime;
    private long lastTime;
    private long elapsedTime;

    private boolean isStopped;
    private boolean isPaused;
    private boolean isRunning;

    public long isFast;//Ratio de velocidad para kinematics

    public KinematicsWatch(){
        this.elapsedTime = 0L;
        this.deltaTime = 0L;
        this.lastTime = 0L;

        this.isStopped = true;
        this.isPaused = false;
        this.isRunning = false;
        this.isFast = 1L;

    }

    public void start(){
        if(isStopped) {
            this.isStopped = false;
            this.isPaused = false;
            this.isRunning = true;

        }
        else{
            //Log.e("kinematicsWatch - Start","Ya estamos corriendo");
        }
    }

    public void pause(){
        if(isRunning && !isPaused) {
            this.isStopped = false;
            this.isPaused = true;
            this.isRunning = false;

            deltaTime = 0L;
            lastTime = 0L;
        }
        else{
            //Log.e("kinematicsWatch - Pause", "Lo siento, no puede ser...");
        }
    }

    public void resume(){
        if(!isRunning && isPaused) {
            this.isStopped = false;
            this.isPaused = false;
            this.isRunning = true;
        }
        else{
            //Log.e("kinematicsWatch-Resume", "Lo siento, no puede ser");
        }
    }

    public void stop(){
        if(!isStopped) {
            this.isStopped = true;
            this.isPaused = false;
            this.isRunning = false;

            elapsedTime = 0L;
            deltaTime = 0L;
            lastTime = 0L;
        }
        else{
            //Log.e("kinematicsWatch - Stop", "Lo siento, no puede ser...");
        }
    }

    public boolean setTime(String t){//setter
        Pattern pattern = Pattern.compile("(\\d{2}):(\\d{2}):(\\d{2}).(\\d{3})");
        Matcher matcher = pattern.matcher(t);
        if (matcher.matches()) {
            elapsedTime = Long.parseLong(matcher.group(1)) * 3600000L
                    + Long.parseLong(matcher.group(2)) * 60000
                    + Long.parseLong(matcher.group(3)) * 1000
                    + Long.parseLong(matcher.group(4));
            return true;
        }return false;//Didn't work!
    }


    public Long getElapsedTime(){return elapsedTime;}


    public String getFormattedTime(){//getter
        DateFormat formatter = new SimpleDateFormat(FORMAT);
        formatter.setTimeZone(TimeZone.getTimeZone("GMT"));//Avoid set up time plus one hour, so annoying!
        return formatter.format(new Date(elapsedTime));
    }



    public long updateTime(){
        if(!isPaused && !isStopped && isRunning) {

            if(lastTime > 0L){
                deltaTime = ((System.currentTimeMillis() - lastTime) * isFast);
                elapsedTime += deltaTime;
            }

            lastTime = System.currentTimeMillis();

            //Log.e("KWatch", "deltaTime = " + deltaTime);
            return deltaTime;
        }
        return -1L;//AKA not working...
    }

    public boolean isPaused() {return isPaused;}
    public boolean isStopped() {return isStopped;}

    public float getDeltaSeconds() {
        return (float)deltaTime / 1000;
    }
}
