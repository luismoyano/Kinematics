package com.kuma.kinematics;

import android.content.Context;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

/**
 * Created by pandolet on 10/05/15.
 */
public class ParentFragment extends Fragment {

    protected final String BAD_NUMBER = "Bad number format, please input a number!";
    protected final String NO_BODY = "Please start or open a project before submitting values!";


    public FragmentData fragmentData;

    /**GestureDetector**/
    GestureDetector gestureDetector;
    DataGestureListener dataGestureListener;

    /**Hijo**/
    private int hijo;

    private boolean screen;

    public ParentFragment(int id, boolean screen) {hijo = id; this.screen = screen;}
    public ParentFragment(){}

    protected KViewModel viewModel = KViewModel.getInstancia();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        Log.e("ParentFragment", "Entro al onCreate");

        fragmentData = new FragmentData(hijo, inflater, container);
        dataGestureListener =  new DataGestureListener(hijo);
        gestureDetector = new GestureDetector(getActivity().getBaseContext(), dataGestureListener);

        fragmentData.getColorableV().setOnTouchListener(
                new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return gestureDetector.onTouchEvent(event);
                    }
                }
        );



        return fragmentData.getV();
    }


    @Override
    public void onResume() {
        super.onResume();

        WindowManager wm = (WindowManager) getActivity().getBaseContext().getSystemService(getActivity().getBaseContext().WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();display.getSize(size);
        int widthData = (size.x);
        int heightData = size.y;
        if (!screen){
            fragmentData.getV().setLayoutParams(new LinearLayout.LayoutParams(((widthData / 6) * 2),
                    ((widthData / 6) * 2)));
            //Log.e("FragmentParent", "Primera opción");
        }
        else{fragmentData.getV().setLayoutParams(new LinearLayout.LayoutParams(widthData,
                widthData));
            //Log.e("FragmentParent", "Segunda opción");
        }
    }

    protected void hideKeyboard(View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(),
                InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }
}
