package com.kuma.kinematics;


import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

/**
 * Created by pandolet on 22/10/13.
 */
public class PositionFragment extends ParentFragment{

    protected static final String X_RECEIVED = "Xp got it!";
    protected static final String Y_RECEIVED = "Yp got it!";

    public PositionFragment(){}
    public PositionFragment(boolean screen) {
        super(0, screen);


    }

    @Override
    public void onResume(){
        super.onResume();

        fragmentData.getxInputCartesian().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                fragmentData.editingX = true;
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    try {
                        viewModel.setBodyPositionX(
                                Float.parseFloat(fragmentData.getxInputCartesian().getText().toString())
                        );
                    } catch (NumberFormatException e) {
                        fragmentData.getxInputCartesian().setHint(R.string.number_hint);
                        Toast.makeText(getActivity().getApplicationContext(), BAD_NUMBER, Toast.LENGTH_SHORT).show();
                    } catch (ArrayIndexOutOfBoundsException e) {
                        fragmentData.getxInputCartesian().setHint(R.string.number_hint);
                        Toast.makeText(getActivity().getApplicationContext(), NO_BODY, Toast.LENGTH_SHORT).show();
                    }
                    hideKeyboard(fragmentData.getV());
                    fragmentData.editingX = false;
                }
                return false;
            }
        });

        fragmentData.getyInputCartesian().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                fragmentData.editingY = true;
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    try {
                        viewModel.setBodyPositionY(
                                Float.parseFloat(fragmentData.getyInputCartesian().getText().toString())
                        );
                    } catch (NumberFormatException e) {
                        fragmentData.getyInputCartesian().setHint(R.string.number_hint);
                        Toast.makeText(getActivity().getApplicationContext(), BAD_NUMBER, Toast.LENGTH_SHORT).show();
                    } catch (ArrayIndexOutOfBoundsException e) {
                        fragmentData.getyInputCartesian().setHint(R.string.number_hint);
                        Toast.makeText(getActivity().getApplicationContext(), NO_BODY, Toast.LENGTH_SHORT).show();
                    }
                    hideKeyboard(fragmentData.getV());
                    fragmentData.editingY = false;
                }
                return false;
            }
        });


        fragmentData.getColorableV().post(new Runnable() {
            @Override
            public void run() {
                viewModel.getData().setPosColorableView(fragmentData.getColorableV());
            }
        });
    }



}
