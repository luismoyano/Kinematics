package com.kuma.kinematics;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Arrays;

/**
 * Created by Luis on 24/05/2015.
 */
public class SaveDialog extends DialogFragment {

    public static final String saveTag = "saveMeUpWhenItsAllOver";
    private final String OK = "Do it!";
    private final String NO = "Nope!";
    private final String inputPlease = "Input some name please!";
    private final String nameInUse = "Name already in use, please type other";

    Context context;
    KViewModel viewModel = KViewModel.getInstancia();

    private View view;

    private EditText txt;

    public SaveDialog(View v){
        context = getActivity();
        view = v;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        context = getActivity();
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view);

        txt = (EditText)view.findViewById(R.id.save_name_container);

        builder.setPositiveButton(OK, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(txt.getText().toString().length() > 0){
                    if(!nameAlreadyExist(txt.getText().toString())){
                        viewModel.saveFile(txt.getText().toString());
                        dismiss();
                    }
                    else{
                        txt.setText("");
                        Toast.makeText(context, nameInUse, Toast.LENGTH_SHORT).show();
                    }
                }
                else{
                    Toast.makeText(context, inputPlease, Toast.LENGTH_SHORT).show();
                }
            }


        });

        builder.setNegativeButton(NO, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dismiss();
            }
        });

        return builder.create();
    }

    private boolean nameAlreadyExist(String n) {
        return Arrays.asList(viewModel.getFileNames()).contains(n);
    }
}
