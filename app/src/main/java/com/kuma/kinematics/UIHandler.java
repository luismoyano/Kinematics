package com.kuma.kinematics;

import android.os.Handler;
import android.os.Message;

import java.lang.ref.WeakReference;

/**
 * Created by Dani on 24/05/2015.
 */
public class UIHandler extends Handler {
    public static final String shouldUpdate = "Go Update!";
    private final WeakReference<KinematicsActivity> mainView;

    private KinematicsActivity activity;
    public UIHandler(WeakReference<KinematicsActivity> mainView) {
        this.mainView = mainView;
        activity = mainView.get();
    }

    @Override
    public void handleMessage(Message message){
        try {

            activity.timeHandler.timeText.setText(message.peekData().getString(KTimeHandler.TIME_SET_CODE));

            if(message.peekData().getBoolean(shouldUpdate)) {
                activity.posFragment.fragmentData.setXText("" + message.peekData().getFloat(PositionFragment.X_RECEIVED));
                activity.posFragment.fragmentData.setYText("" + message.peekData().getFloat(PositionFragment.Y_RECEIVED));

                activity.velFragment.fragmentData.setXText("" + message.peekData().getFloat(VelocityFragment.X_RECEIVED));
                activity.velFragment.fragmentData.setYText("" + message.peekData().getFloat(VelocityFragment.Y_RECEIVED));

                activity.accFragment.fragmentData.setXText("" + message.peekData().getFloat(AccelerationFragment.X_RECEIVED));
                activity.accFragment.fragmentData.setYText("" + message.peekData().getFloat(AccelerationFragment.Y_RECEIVED));
            }
        }catch (Exception e){
            //Nothing
        }
        super.handleMessage(message);

    }
}
