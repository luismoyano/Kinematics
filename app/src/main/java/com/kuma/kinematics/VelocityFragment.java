package com.kuma.kinematics;


import android.view.KeyEvent;
import android.view.View;
import android.widget.Toast;

/**
 * Created by pandolet on 22/10/13.
 */
public class VelocityFragment extends ParentFragment{

    protected static final String X_RECEIVED = "Xv got it!";
    protected static final String Y_RECEIVED = "Yv got it!";

    public VelocityFragment(){}

    public VelocityFragment(boolean screen) {
        super(1, screen);


    }

    @Override
    public void onResume(){
        super.onResume();

        fragmentData.getxInputCartesian().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                fragmentData.editingX = true;
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    try {
                        viewModel.setBodyVelocityX(
                                Float.parseFloat(fragmentData.getxInputCartesian().getText().toString())
                        );
                    } catch (NumberFormatException e) {
                        fragmentData.getxInputCartesian().setHint(R.string.number_hint);
                        Toast.makeText(getActivity().getApplicationContext(), BAD_NUMBER, Toast.LENGTH_SHORT).show();
                    } catch (ArrayIndexOutOfBoundsException e) {
                        fragmentData.getxInputCartesian().setHint(R.string.number_hint);
                        Toast.makeText(getActivity().getApplicationContext(), NO_BODY, Toast.LENGTH_SHORT).show();
                    }
                    hideKeyboard(fragmentData.getV());
                    fragmentData.editingX = false;
                }
                return false;
            }
        });

        fragmentData.getyInputCartesian().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                fragmentData.editingY = true;
                if (keyCode == KeyEvent.KEYCODE_ENTER) {
                    try {
                        viewModel.setBodyVelocityY(
                                Float.parseFloat(fragmentData.getyInputCartesian().getText().toString())
                        );
                    } catch (NumberFormatException e) {
                        fragmentData.getyInputCartesian().setHint(R.string.number_hint);
                        Toast.makeText(getActivity().getApplicationContext(), BAD_NUMBER, Toast.LENGTH_SHORT).show();
                    } catch (ArrayIndexOutOfBoundsException e) {
                        fragmentData.getyInputCartesian().setHint(R.string.number_hint);
                        Toast.makeText(getActivity().getApplicationContext(), NO_BODY, Toast.LENGTH_SHORT).show();
                    }
                    hideKeyboard(fragmentData.getV());
                    fragmentData.editingY = false;
                }
                return false;
            }
        });

        fragmentData.getColorableV().post(new Runnable() {
            @Override
            public void run() {
                viewModel.getData().setVelColorableView(fragmentData.getColorableV());
            }
        });
    }

}
